﻿// See https://aka.ms/new-console-template for more information
using System.Diagnostics;
using System.Dynamic;
using System.Text.Json;
using w4;
using w4.Logger;
using w4.Serial;

Stopwatch stopWatch = new Stopwatch();
TimeSpan ts;
int countIteration = 10000; 

ISerialize serialize = new CSV();
ILogger logger = new WriteToConsole();

var serializeObject = new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
var deserializeObject = new F();

logger.WriteToLog($"количество замеров = {String.Format("{0:0}", countIteration)}");
logger.WriteToLog($"мой рефлекшен:");
stopWatch.Start();
for (int i = 0; i < countIteration; i++)
{
    serialize.Serialize(serializeObject);
}
stopWatch.Stop();
logger.WriteToLog($"Время на сериализацию = {String.Format("{0:0.####}", stopWatch.Elapsed.TotalMilliseconds/countIteration)}");

stopWatch.Start();
for (int i = 0; i < countIteration; i++)
{
    serialize.Deserialize(deserializeObject);
}
stopWatch.Stop();
logger.WriteToLog($"Время на десериализацию = {String.Format("{0:0.####}", stopWatch.Elapsed.TotalMilliseconds / countIteration)}");



logger.WriteToLog($"стандартный механизм(System.Json):");

serialize = new JSON();
stopWatch.Start();
serialize.Serialize(serializeObject);
stopWatch.Stop();
logger.WriteToLog($"Время на сериализацию = {String.Format("{0:0.####}", stopWatch.Elapsed.TotalMilliseconds/countIteration)}");
stopWatch.Start();
serialize.Deserialize(deserializeObject);
stopWatch.Stop();
logger.WriteToLog($"Время на десериализацию = {String.Format("{0:0.####}", stopWatch.Elapsed.TotalMilliseconds / countIteration)}");


Console.ReadKey();
