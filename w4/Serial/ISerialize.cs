﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w4.Serial
{
    public interface ISerialize
    {
        void Serialize(object message);
        public object Deserialize(object message);
    }
}
