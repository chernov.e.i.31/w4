﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace w4.Serial
{
    public class JSON : ISerialize
    {
        public void Serialize(object elem)
        {
            var sObject = JsonSerializer.Serialize(elem);
            File.WriteAllText("file.json", sObject);
        }

        public object Deserialize(object elem)
        {
            var file = File.ReadAllText("file.json");
            var json = (F)JsonSerializer.Deserialize<F>(file);
            return json;
        }
    }
}
