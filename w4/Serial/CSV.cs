﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w4.Serial
{
    public class CSV : ISerialize
    {
        public void Serialize(object elem)
        {
            List<string> result = new List<string>();
            var fields = elem.GetType().GetFields();
            result.Add(string.Join(";", fields.Select(x => x.Name)));
            result.Add(string.Join(";", fields.Select(x => x.GetValue(elem))));
            File.WriteAllLines($@"classList.csv", result);
        }

        public object Deserialize(object elem)
        {
            var values = File.ReadAllLines($@"classList.csv").ToList();
            var fields = elem.GetType().GetFields();
            var array = values.Skip(1).FirstOrDefault().Split(";");
            int index = 0;

            foreach (var f in fields)
            {
                int res = 0;
                int.TryParse(array[index], out res);
                f.SetValue(elem, res);
                index++;
                if (array.Length < index) { break; }
            }
            return elem;
        }


    }
}
