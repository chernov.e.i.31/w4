﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w4.Logger
{
    public interface ILogger
    {
        void WriteToLog(string message);
    }
}
